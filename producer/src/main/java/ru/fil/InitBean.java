package ru.fil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

/**
 * User: fil
 * Date: 20.10.15
 */
@Singleton
@Startup
public class InitBean {

    private final Logger log = LoggerFactory.getLogger(getClass().getName());

    @Inject
    private TestJMSSender testJMSSender;
    @Inject
    private TestEJBSender testEJBSender;

    @PostConstruct
    public void init(){
        log.info("Start producer init...");
//        testJMSSender.run();
        testEJBSender.run();
        log.info("Producer initialized.");
    }

}
