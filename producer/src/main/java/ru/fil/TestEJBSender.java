package ru.fil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.fil.ejb.RpcApiProxy;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;

/**
 * User: fil
 * Date: 20.10.15
 */
@ApplicationScoped
public class TestEJBSender {

    private final Logger log = LoggerFactory.getLogger(getClass().getName());

    @EJB
    private RpcApiProxy rpcApiProxy;

    private boolean active = true;

    public void run(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int numberCounter = -1;
                    while(active){
                        try {
                            rpcApiProxy.log(String.valueOf(++numberCounter));
                            log.info("tick: "+numberCounter);
                        }catch (Exception e){
                            log.error("Cant send tick: "+numberCounter);
                        }
                        Thread.sleep(5000);
                    }
                } catch (Exception e) {
                    log.error("Unexpected thread EJB execute!", e);
                }
            }
        });
        thread.start();
    }

    @PreDestroy
    public void destroy(){
        active = false;
    }

}
