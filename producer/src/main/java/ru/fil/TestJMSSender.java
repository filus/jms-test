package ru.fil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.enterprise.context.ApplicationScoped;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Queue;
import javax.naming.InitialContext;
import javax.transaction.UserTransaction;
import java.util.UUID;
import java.util.concurrent.ThreadFactory;

/**
 * User: fil
 * Date: 20.10.15
 */
@ApplicationScoped
public class TestJMSSender {

    private static final String JMS_REMOTE_CONNECTION = "JmsXA";

    private final Logger log = LoggerFactory.getLogger(getClass().getName());

    @Resource(lookup= JMS_REMOTE_CONNECTION)
    private ConnectionFactory connectionFactory;
//    @Resource
//    private EJBContext context;

    private boolean active = true;

    public void run(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    InitialContext initCxt = new InitialContext();
                    Queue queue = (Queue) initCxt.lookup("jms/queue/filTestQueue");
                    int numberCounter = 0;
                    while(active){
//                        UserTransaction utx = context.getUserTransaction();
//                        try {
//                            utx.begin();
                            try (JMSContext jmsContext = connectionFactory.createContext(/*JMSContext.SESSION_TRANSACTED*/)) {
                                Event event = new Event(UUID.randomUUID().toString(), numberCounter);
                                jmsContext.createProducer().send(queue, event);
                                log.info("send: " + event);
                            }catch (Exception e){
                                log.error("JMS message send exception!", e);
                            }
//                            if(numberCounter % 3 == 0){
//                                throw new RuntimeException("======== rollback it! ========");
//                            }
//                            utx.commit();
//                        }catch (Exception e){
//                            utx.rollback();
//                        }
                        Thread.sleep(20000);
                        numberCounter++;
                    }
                } catch (Exception e) {
                    log.error("Unexpected JMS send!", e);
                }
            }
        });
        thread.start();
    }

    @PreDestroy
    public void destroy(){
        active = false;
    }

}
