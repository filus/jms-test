package ru.fil.ejb;

import javax.ejb.Local;
import javax.ejb.Singleton;

/**
 * remote EJB принято внедрять в одном месте
 * User: fil
 * Date: 28.04.15
 */
@Singleton
@Local(RpcApiProxy.class)
public class RpcApiBean extends RpcApiProxyImpl {
}
