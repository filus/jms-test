package ru.fil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

/**
 * User: fil
 * Date: 20.10.15
 */
@MessageDriven(name = "FilTestQueue", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "java:"+TestMDB.JMS_NAME),
        @ActivationConfigProperty(propertyName = "minSession", propertyValue = "1"),
        @ActivationConfigProperty(propertyName = "maxSession", propertyValue = "4")
})
@TransactionManagement(TransactionManagementType.CONTAINER)
public class TestMDB implements MessageListener {

    public static final String JMS_NAME = "jms/queue/filTestQueue";

    private final Logger log = LoggerFactory.getLogger(getClass().getName());

    @PostConstruct
    public void init(){
//        log.info("TestMDB initialized");
    }

    public void onMessage(Message message) {
        try {
            Event event = message.getBody(Event.class);
            log.info("try: "+message.getIntProperty("JMSXDeliveryCount")+" event: "+event);
//            throw new RuntimeException("test exception");
            log.info("receive: "+event);
        } catch (JMSException e) {
            log.error("Unexpected JMS receive!", e);
        }
    }

}
