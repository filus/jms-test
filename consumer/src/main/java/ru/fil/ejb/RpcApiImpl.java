package ru.fil.ejb;

import org.jboss.ejb3.annotation.Clustered;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

/**
 * @author fil
 */
@Stateless
@Remote(RpcApi.class)
//@Clustered
@TransactionManagement(TransactionManagementType.CONTAINER)
public class RpcApiImpl implements RpcApi {

    private Logger log = LoggerFactory.getLogger(getClass().getName());

    @Override
    public void log(String id) {
        log.info("ping " + id);
    }
}
