package ru.fil;

import java.io.Serializable;

/**
 * User: fil
 * Date: 20.10.15
 */
public class Event implements Serializable {

    private String uid;
    private long number;

    public Event(){}

    public Event(String uid, long number){
        setUid(uid);
        setNumber(number);
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName()+" {"+
                "uid: " + uid+
                ", number: " + number+
                "}";
    }
}
