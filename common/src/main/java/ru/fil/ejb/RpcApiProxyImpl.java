package ru.fil.ejb;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.ejb.Stateless;

/**
 * remote EJB принято внедрять в одном месте, обернем его в singleton
 * @author fil
 */
public class RpcApiProxyImpl implements RpcApiProxy {

    @EJB(lookup = "ejb:jms-test-consumer/consumer/RpcApiImpl!ru.fil.ejb.RpcApi")
    private RpcApi rpcApi;

    @Override
    public void log(String id) {
        rpcApi.log(id);
    }
}
